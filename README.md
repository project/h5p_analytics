# H5P Analytics (h5p_analytics)

A Drupal 8+ integration of Experience API (xAPI) statements emitted by H5P content types to be captured and sent to Learning Record Store (LRS).

Includes a `h5p_analytics_tip` submodule that patches the `JoubelUI.createTip` method to determine if the tip is
opened/closed and send `opened-tip` or `closed-tip` xAPI statement with standard statement structure, help text data
included as an extension and a duration data included as an extension for tip being closed.

## Requirements

* PHP 7.4+
* Drupal 9 or 10
* H5P module for Drupal

## Installation

* Add to `/modules` directory and activate
* Fill in the LRS configuration data (please note that larger batches might require more memory being available to the process)
* Set up the cron job to be triggered every 30 minutes (the internal sending logic is allowed to run for about 20 minutes)
* (Optional) Enabled the `h5p_analytics_tip` submodule to get tip xAPI statements.

## General logic

Module integrates with H5P on the client side (covering both internal content within normal pages and an externally embedded one).
The xAPI event listener is being set up and statements are sent to the backend. The backend is capturing statements and adding those to a queue.
A periodic background process will go through the queue and combine individual statements into batches with configurable size.
Those batches would in turn be processed by the `BatchQueue` job and sent to the LRS, if possible.
All HTTP requests would have their failures logged, with probability of the same batch appearing multiple times under specific circumstances.
Statistical data for failed or successful requests would be added to a standalone log (failures would include JSON-encoded statements batch).

## Custom statements

```json
{
    "actor": {
        "mbox": "mailto:someone@example.com",
        "name": "someone",
        "objectType": "Agent"
    },
    "context": {
        "contextActivities": {
            "category": [
                {
                    "id": "http://h5p.org/libraries/H5P.InteractiveBook-1.6",
                    "objectType": "Activity"
                }
            ]
        }
    },
    "object": {
        "definition": {
            "extensions": {
                "http://h5p.org/x-api/h5p-local-content-id": 5,
                "https://h5p.ee/xapi/extensions/tip-text": "hint text"
            },
            "name": {
                "en-US": "Material title"
            }
        },
        "id": "http://localhost/h5p/5/embed",
        "objectType": "Activity"
    },
    "verb": {
        "display": {
            "en-US": "opened+tip"
        },
        "id": "https://h5p.ee/xapi/verbs/opened-tip"
    }
}
```

```json
{
    "actor": {
        "mbox": "mailto:someone@example.com",
        "name": "someone",
        "objectType": "Agent"
    },
    "context": {
        "contextActivities": {
            "category": [
                {
                    "id": "http://h5p.org/libraries/H5P.InteractiveBook-1.6",
                    "objectType": "Activity"
                }
            ]
        }
    },
    "object": {
        "definition": {
            "extensions": {
                "http://h5p.org/x-api/h5p-local-content-id": 5,
                "https://h5p.ee/xapi/extensions/duration": "PT5S",
                "https://h5p.ee/xapi/extensions/tip-text": "hint text"
            },
            "name": {
                "en-US": "Material title"
            }
        },
        "id": "http://localhost/h5p/5/embed",
        "objectType": "Activity"
    },
    "verb": {
        "display": {
            "en-US": "closed+tip"
        },
        "id": "https://h5p.ee/xapi/verbs/closed-tip"
    }
}
```

## TODO

* Add better handling of different response cases (come up with a solution for request timeout).
* See if it would make sense to remove the statement data from the request log after a certain period of time (storing that indefinitely seems wasteful and pointless).
* Add token checks to the xAPI statements AJAX endpoint so that it could not be easily spammed or at least protect from outside requests
* Allow downloading statement log and request log data as CSV (having possibility to select a period is a plus)
* Consider allowing statistics page to have data with period limitation capability

## Issues

* Handling of LRS responses is incomplete. Current solution might not be able to handle all the meaningful HTTP call failures that would allow retrying with the same batch (a few cases are handled, but that should be a default behaviour).
* Logging is eager and stores full batch dataset on each failed HTTP request. Might need to be discontinued or automatically removed by a cleanup procedure.
